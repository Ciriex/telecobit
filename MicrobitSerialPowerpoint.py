#
from microbit import *
import radio

#activation de la radio
def set_up():
    radio.on()


def loop():
    #reception du message radio
    message = radio.receive()
    #si la variable message est rempli ecrire le message en serie
    if message:
        uart.write(message + "\n")

set_up()

while(True):
    loop()