#PC traitement !
#importation
from pynput import keyboard
from pynput.keyboard import *
import serial
import sys

#Declaration et affectation des variables
PORT = 'COM6'
BAUD = 115200

keyboard_c = Controller()
#config
s = serial.Serial(PORT)
s.baudrate = BAUD
s.parity  = serial.PARITY_NONE
s.databits = serial.EIGHTBITS
s.stopbits = serial.STOPBITS_ONE

#traitement du message reçu
while(True):
    message = s.readline()
    message = message.decode('utf-8')
    message = message.strip()

#si le message est "a" appuyer sur la flèche de gauche
    if message == "a":
        keyboard_c.press(keyboard.Key.left)
#sinon appuyer sur la flèche de droite
    else:
        keyboard_c.press(keyboard.Key.right)


s.close()