#Microbit qui envoi par radio
from microbit import *
import radio

#activation de la radio
def set_up():
    radio.on()



#envoi d'un message par radio
def loop():
    #si le boutton "a" est presser envoyer le message "a"
    if button_a.is_pressed():
        display.scroll("L")
        radio.send("a")
        #Sinon si le boutton "b" est presser envoyer le message "b"
    elif button_b.is_pressed():
        display.scroll("R")
        radio.send("b")

set_up()

while(True):
    loop()