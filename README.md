La t�l�cobit est une carte microbit d�tourn� en t�l�commande. La fonction principale est de pouvoir s'en servir pour passer les diaporamas d'un powerpoint. Pour cela nous avons eu 
besoin de deux Microbit, une branch� en s�rie sur un ordinateur et une qui fera office de t�l�commande.

Le projet se d�coupe en trois fichiers : 

_ MicrobitSerialPowerpoint.py
_ MicrobitRadioPowerpoint.py
_ PCPowerpoint.py

Le fichier MicrobitSerialPowerpoint.py sera a flasher dans la Microbit qui sera branch� en s�rie sur le pc.

Le fichier MicrobitRadioPowerpoint.py sera a flasher dans la Microbit qui servira de t�l�commande.

Le fichier PCPowerpoint.py servira pour le pc sur lequel la Microbit en s�rie sera branch�. Il faut le laiss� tourner en arri�re plan sur cet ordinateur.

